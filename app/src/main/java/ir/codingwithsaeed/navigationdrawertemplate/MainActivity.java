package ir.codingwithsaeed.navigationdrawertemplate;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.material.navigation.NavigationView;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private Toolbar toolbar;
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private TextView txt_navHeaderName;
    private NavMenuState navMenuState = NavMenuState.DOWN;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_layout);
        initViews();
    }

    private void initViews() {
        toolbar = findViewById(R.id.toolbar_main);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.title);

        //setup navigation
        drawerLayout = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.navigation_view);
        txt_navHeaderName = navigationView.getHeaderView(0).findViewById(R.id.txt_nav_header_name);
        navigationView.setNavigationItemSelectedListener(this);
        setMenuCounter(R.id.nav_inbox, 0);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close
        );
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        initListeners();
    }

    private void initListeners() {
        txt_navHeaderName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (navMenuState == NavMenuState.DOWN) {
                    txt_navHeaderName.setBackgroundResource(R.drawable.bg_spinner_drop_up);
                    navMenuState = NavMenuState.UP;
                    navigationView.getMenu().clear();
                    navigationView.inflateMenu(R.menu.navigation_menu_mini);
                    return;
                }
                txt_navHeaderName.setBackgroundResource(R.drawable.bg_spinner_drop_down);
                navMenuState = NavMenuState.DOWN;
                navigationView.getMenu().clear();
                navigationView.inflateMenu(R.menu.navigation_menu);
            }
        });
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_inbox:
                Toast.makeText(this, item.getTitle(), Toast.LENGTH_SHORT).show();
                break;
            case R.id.nav_stared:
                Toast.makeText(this, item.getTitle(), Toast.LENGTH_SHORT).show();
                break;

            case R.id.nav_send:
                Toast.makeText(this, item.getTitle(), Toast.LENGTH_SHORT).show();
                break;

            case R.id.nav_drafts:
                Toast.makeText(this, item.getTitle(), Toast.LENGTH_SHORT).show();
                break;


            default:
                Toast.makeText(this, item.getTitle(), Toast.LENGTH_SHORT).show();
                navigationView.clearFocus();

        }
        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    private void setMenuCounter(@IdRes int itemId, int count) {
        TextView view = (TextView) navigationView.getMenu().findItem(itemId).getActionView();
        view.setText(count >= 0 ? String.valueOf(count) : null);
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
            return;
        }

        super.onBackPressed();
    }
}